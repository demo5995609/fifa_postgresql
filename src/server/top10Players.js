function top10Players() {

  return `SELECT  playername,
                    
                    ROUND((SUM(CASE WHEN Event LIKE '%G%' THEN 1 ELSE 0 END) * 100.0 / COUNT(*))::numeric, 2) AS probability
                    FROM worldcupplayers
                    GROUP BY playername
                    HAVING COUNT(*) > 5
                    ORDER BY probability DESC limit 10;
                    `;
  
}

module.exports = top10Players;
