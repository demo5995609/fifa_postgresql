function wonPerTeams() {
  let homeTeamQuery = `SELECT 
                    hometeamname AS team_name, 
                    COUNT(CASE 
                            WHEN hometeamgoals > awayteamgoals then 1
                          END
                          ) as team_wins
                    from worldcupmatches group by team_name`;
  let awayTeamQuery = `select
                    awayteamname AS team_name,
                    count(case
                            when awayteamgoals>hometeamgoals then 1
                          END
                          ) as team_wins
                          from worldcupmatches group by team_name`;
  
  return `${homeTeamQuery} union all ${awayTeamQuery}`;
}


module.exports = wonPerTeams;