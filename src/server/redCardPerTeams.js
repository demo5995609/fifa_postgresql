function redTeams() {
  return `select worldcupplayers.teaminitials, count(worldcupplayers.teaminitials) 
                      from worldcupplayers inner join worldcupmatches on worldcupplayers.matchid = worldcupmatches.matchid 
                        where (worldcupplayers.event like '%R%' and worldcupmatches.year = 2014) 
                          group by worldcupplayers.teaminitials`;
 
}

module.exports = redTeams;
