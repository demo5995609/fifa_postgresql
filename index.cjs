const { Client } = require("pg");
const fs = require("fs");
const findMatchesperCities = require("./src/server/findMatchesPerCity.js");
const wonPerTeams = require("./src/server/matchesPerTeams.js");
const redTeams = require("./src/server/redCardPerTeams.js");
const top10Players = require("./src/server/top10Players.js");

const path = require("path");
const client = new Client({
  host: "localhost",
  user: "postgres",
  port: 5432,
  password: "hp",
  database: "pradeep",
});
client.connect();

// Finding Matches Per Cities

client.query(findMatchesperCities(), (err, res) => {
  console.log(res.rows);
  fs.writeFileSync(
    path.join(__dirname, "/src/public/output/matchesPerCities.json"),
    JSON.stringify(res.rows),
    "utf-8"
  );
});

// Finding winning per Teams

client.query(wonPerTeams(), (err, res) => {
  console.log(res.rows);
  fs.writeFileSync(
    path.join(__dirname, "/src/public/output/wonPerTeams.json"),
    JSON.stringify(res.rows),
    "utf-8"
  );
});

// Finding Red Teams of 2014

client.query(redTeams(), (err, res) => {
  console.log(res.rows);
  fs.writeFileSync(
    path.join(__dirname, "/src/public/output/redCardPerTeams.json"),
    JSON.stringify(res.rows),
    "utf-8"
  );
});

// Finding top 10 players according to probability of scores

client.query(top10Players(), (err, res) => {
  console.log(res.rows);
  fs.writeFileSync(
    path.join(__dirname, "/src/public/output/top10Players.json"),
    JSON.stringify(res.rows),
    "utf-8"
  );
});
